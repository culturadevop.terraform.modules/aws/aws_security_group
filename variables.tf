variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = list(object({
    name   = string
    vpc_id = string
    tags   = optional(map(string))
    ingress = list(object({
      from_port : number
      to_port : number
    }))
    egress = optional(list(object({
      from_port : number
      to_port : number
    })))
  }))
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
